import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeroesComponent } from './heroes.component';
import { HeroesListComponent } from './heroes-list/heroes-list.component';
import { HeroesNewComponent } from './heroes-new/heroes-new.component';
import { HeroesDetailComponent } from './heroes-detail/heroes-detail.component';

const routes: Routes = [
  {
    path: '',
    component: HeroesComponent,
    children: [
      { path: '', component: HeroesListComponent },
      { path: 'new', component: HeroesNewComponent },
      {
        path: ':id',
        component: HeroesDetailComponent,
      },
      {
        path: ':id/edit',
        component: HeroesDetailComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeroesRoutingModule {}
