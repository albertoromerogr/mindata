import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Hero } from 'src/app/interfaces/hero.interface';
import { HeroesService } from 'src/app/services/heroes.service';

@Component({
  selector: 'app-heroes-list',
  templateUrl: './heroes-list.component.html',
  styleUrls: ['./heroes-list.component.css']
})

export class HeroesListComponent implements OnInit, OnDestroy {
  heroes: Hero[] = [];
  dataSource: Hero[] = [];
  subscription!: Subscription;
  displayedColumns: string[] = ['id', 'name', 'team', 'actions'];

  constructor(private heroService: HeroesService) { }

  onDelete = (id: number) => {
    this.heroService.deleteHero(id);
  }

  ngOnInit(): void {
    this.subscription = this.heroService.heroesChanged.subscribe(
      (heroes: Hero[]) => {
        this.heroes = heroes;
      }
    );
    this.heroes = this.heroService.getHeroes();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
