import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';


import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';

import { HeroesRoutingModule } from "./heroes-routing.module";
import { HeroesComponent } from './heroes.component';
import { HeroesListComponent } from './heroes-list/heroes-list.component';
import { HeroesDetailComponent } from './heroes-detail/heroes-detail.component';
import { HeroesNewComponent } from './heroes-new/heroes-new.component';

import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

import { CapitalizePipe } from '../pipes/capitalize.pipe';
import { AutosearchPipe } from '../pipes/autosearch.pipe';


@NgModule({
  declarations: [
    HeroesComponent,
    HeroesListComponent,
    HeroesDetailComponent,
    HeroesNewComponent,
    CapitalizePipe,
    AutosearchPipe
  ],
  imports: [
    RouterModule,
    ReactiveFormsModule,
    CommonModule,
    HeroesRoutingModule,
    MatButtonModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatTableModule
  ]
})
export class HeroesModule {}
