import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroesService } from 'src/app/services/heroes.service';

@Component({
  selector: 'app-heroes-new',
  templateUrl: './heroes-new.component.html',
  styleUrls: ['./heroes-new.component.css']
})
export class HeroesNewComponent implements OnInit {
  heroForm!: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: HeroesService) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.heroForm = new FormGroup({
      'name': new FormControl('', Validators.required),
      'description': new FormControl('', Validators.required),
      'team': new FormControl('', Validators.required),
      'image': new FormControl('', Validators.required)
    });
  }

  onSubmit = () => {
    if (this.heroForm.valid) {
      this.service.addHero(this.heroForm.value);
      this.router.navigate(['../'], {relativeTo: this.route});
    }
  };

}
