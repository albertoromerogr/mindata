import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Hero } from 'src/app/interfaces/hero.interface';
import { HeroesService } from 'src/app/services/heroes.service';

@Component({
  selector: 'app-heroes-detail',
  templateUrl: './heroes-detail.component.html',
  styleUrls: ['./heroes-detail.component.css']
})
export class HeroesDetailComponent implements OnInit {
  heroForm: FormGroup = new FormGroup({
    'id': new FormControl(0),
    'name': new FormControl('', Validators.required),
    'description': new FormControl('', Validators.required),
    'team': new FormControl('', Validators.required),
    'image': new FormControl('', Validators.required)
  });
  id: number = 0;
  editMode = false;
  heroes!: Hero[];
  hero!: Hero;
  recipeForm!: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: HeroesService) { }

  ngOnInit(): void {
    this.heroes = this.service.getHeroes();
    this.route.url.subscribe(
      (value) => {
        this.editMode = value[1] != undefined;
      }
    );
    this.route.params.subscribe(
      (params) => {
        if (params['id'] !== undefined) {
          this.id = +params['id'];
          const index = this.heroes.findIndex( (hero) => {
            return hero.id == this.id
          });
          this.hero = this.heroes[index];
          this.initForm();
        }
      }
    )
  }

  private initForm() {
    this.heroForm = new FormGroup({
      'id': new FormControl(this.hero.id),
      'name': new FormControl(this.hero.name, Validators.required),
      'description': new FormControl(this.hero.description, Validators.required),
      'team': new FormControl(this.hero.team.toLowerCase(), Validators.required),
      'image': new FormControl(this.hero.image, Validators.required)
    });
  }

  onSubmit = () => {
    if (this.heroForm.valid) {
      this.service.editHero(this.heroForm.value);
      this.router.navigate(['/']);
    }
  };

}
