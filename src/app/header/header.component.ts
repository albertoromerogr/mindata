import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../loading/loading.service';
import { DataStorageService } from '../services/data-storage.service';
//import { Subscription } from "rxjs";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  appName: string = 'Heroes App';

  constructor(
    private dataStorageService: DataStorageService,
    private loadingService: LoadingService
    ) { }

  ngOnInit(): void {
  }

  onFetchData = (): void => {
    this.loadingService.load();
    this.dataStorageService.fetchRecipes().subscribe();
  }

}
