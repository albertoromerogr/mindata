import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { HeroesService } from "./heroes.service";
import { Hero } from "../interfaces/hero.interface";
import { map, tap } from "rxjs/operators";

@Injectable({ providedIn: 'root' })
export class DataStorageService {
  constructor(private http: HttpClient, private heroesService: HeroesService) { }

  fetchRecipes = () => {
    const fileUrl = "assets/heroes.json";
    return this.http.get<Hero[]>(
      fileUrl,
    ).pipe(
      map(heroes => {
        return heroes
      }),
      tap(heroes => {
        this.heroesService.setHeroes(heroes);
      })
    );
  }
}
