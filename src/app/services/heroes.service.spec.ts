import { TestBed } from "@angular/core/testing";
import { HeroesService } from "./heroes.service";

let service: HeroesService;

beforeEach(() => {
  TestBed.configureTestingModule({ providers: [HeroesService] });
});

it('should use HeroesService', () => {
  service = TestBed.inject(HeroesService);
  expect(service.getHeroes()).toBeDefined;
});
