import { Injectable } from '@angular/core';
import { Hero } from "../interfaces/hero.interface";
import { Subject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class HeroesService {

  heroesChanged = new Subject<Hero[]>();
  private heroes: Hero[] = [];

  constructor() {}

  getHeroes( ){
    return this.heroes.slice();
  }

  setHeroes(heroes: Hero[]) {
    this.heroes = heroes;
    this.heroesChanged.next(this.heroes.slice());
  }

  addHero(hero: Hero) {
    hero.id = this.heroes.length + 1;
    this.heroes.push(hero);
    this.heroesChanged.next(this.heroes.slice());
  }

  editHero(hero: Hero) {
    const index = this.heroes.findIndex( (h) => {
      return h.id == hero.id
    });
    this.heroes[index] = hero;
    this.setHeroes(this.heroes);
  }

  deleteHero(id: number) {
    const index = this.heroes.findIndex( (hero) => {
      return hero.id == id
    });
    this.heroes.splice(index, 1);
    this.setHeroes(this.heroes);
  }

}
