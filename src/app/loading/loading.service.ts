// Service Dependencies
import { Injectable } from '@angular/core';

@Injectable()
export class LoadingService {

  public start = false;

  constructor(
  ) {}

  public load(time = 2000) {
    this.start = true;
    setTimeout(() => this.start = false, time);
  }

  public checkNotification() {
    return this.start;
  }
}
