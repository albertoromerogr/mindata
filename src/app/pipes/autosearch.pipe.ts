import { Pipe, PipeTransform } from '@angular/core';
import { Hero } from '../interfaces/hero.interface';

@Pipe({
  name: 'autosearch'
})
export class AutosearchPipe implements PipeTransform {

  transform(heroes: Hero[], feed:string ): any{

    if (feed.length < 3 || heroes.length <2) {
      return heroes;
    }

    return heroes.filter(hero => hero.name.toLowerCase().search(feed.toLowerCase()) > -1);

  }

}
