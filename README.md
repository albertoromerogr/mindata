# Mindata test

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Start

To start the aplication click into `Get Data` button

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Run in Docker

Open terminal and go into de mindata folder

Run `docker build -t mindata .`

Run `docker run -d -it -p 80:80 mindata`

## Open

With docker:  <http://localhost>

With angular cli: <http://localhost:4200>
